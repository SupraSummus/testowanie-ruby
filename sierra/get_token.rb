#!/bin/ruby

require 'net/http'

client_key = "client_key"
client_secret = "client_secret"
domain = "https://alpha.bn.org.pl/iii/sierra-api/v0.5/"

url = URI.parse(domain + "token")

req = Net::HTTP::Post.new(url.path)
req.basic_auth(client_key, client_secret)
req.set_content_type("application/x-www-form-urlencoded")
req.set_form_data({"grant_type" => "client_credentials"})

res = Net::HTTP.start(url.host, url.port, :use_ssl => url.scheme == 'https') do |http|
	http.request(req)
end
 
puts res.body
