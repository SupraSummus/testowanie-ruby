#!/bin/ruby

class Hash
	def method_missing(name, *args, &block)
		if name.to_s.end_with?("=")
			# insert
			store(name.to_s.chop.to_sym, args[0])
		elsif has_key?(name)
			# get
			return fetch(name)
		else
			super
		end
	end
end

mapa = {:zero => 0}

puts mapa[:zero]
puts mapa.zero

mapa.store(:mkey,"value")
puts mapa.mkey

mapa.mkey2 = "he"
puts mapa.mkey2
puts mapa[:mkey2]



