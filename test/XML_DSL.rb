#!/bin/ruby

DEBUG = false

class XMLLeafBuilder
	def initialize(text)
		if DEBUG
			puts "nowy XMLLeafBuilder| text: #{text}"
		end
		@text = text
	end
	
	def render
		return @text
	end
end

class XMLHasChildrenBuilder
	def initialize(&block)
		if block != nil
			@children = []
			instance_eval(&block)
		end
	end
	
	def method_missing(name, *args, &block)
		@children.push(XMLTagBuilder.new(name.to_s, *args, &block))
	end
	
	def _(text)
		@children.push(XMLLeafBuilder.new(text))
	end
	
	def render
		if @children != nil
			return @children.map{ |child| child.render }.join
		else
			return ""
		end
	end
end

class XMLTagBuilder < XMLHasChildrenBuilder
	def initialize(name, *args, &block)
		if DEBUG
			puts "nowy XMLTagBuilder| name: #{name}, args: #{args.inspect}, block #{block.inspect}"
		end
		
		@name = name
		@attrs = args[0]
		
		super(&block)
	end
	
	def render
		t = []
		t.push("<", @name)
		
		if @attrs != nil
			@attrs.each{|key, value| t.push(" ", key.to_s, "='", value, "'")}
		end
		
		if @children != nil
			t.push(">")
			t.push(super)
			t.push("</", @name)
		else
			t.push(" /")
		end
		
		t.push(">")
		return t.join
	end
end

class XMLRootBuilder < XMLHasChildrenBuilder
end

doc = XMLRootBuilder.new do
	html do
		head {
			title {_"Lorem ipsum"}
		}
		body do
			h1(style: 'color: red') {_'Dolor sit amet'}
		end
	end
end

puts doc.render
