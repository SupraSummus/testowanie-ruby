#!/bin/ruby

load 'XML_DSL.rb'

describe XMLLeafBuilder do
	it "renders to string passed in constructor" do
		leaf = XMLLeafBuilder.new("text value")
		expect(leaf.render).to eq("text value")
	end
end

describe XMLTagBuilder do
	it "renders to <tagname /> when empty" do
		tag = XMLTagBuilder.new("tagname")
		expect(tag.render).to eq("<tagname />")
	end
	
	it "renders to <tagname></tagname> when empty block is passed to constructor" do
		tag = XMLTagBuilder.new("tagname") do end
		expect(tag.render).to eq("<tagname></tagname>")
	end
	
	it "renders to <tagname>joined rendered children</tagname>" do
		tag = XMLTagBuilder.new("tagname") do
			a
			_"text value"
			_" hyhy"
			b do
				_"hay"
			end
			c
		end
		expect(tag.render).to eq(
			"<tagname>" +
			XMLTagBuilder.new("a").render +
			XMLLeafBuilder.new("text value").render +
			XMLLeafBuilder.new(" hyhy").render +
			(XMLTagBuilder.new("b") do
				_"hay"
			end).render +
			XMLTagBuilder.new("c").render +
			"</tagname>"
		)
	end
	
	it "can have parameters and no children" do
		tag = XMLTagBuilder.new("tagname", {"p1" => "v1", "p2" => "v2", "p3" => "v3"})
		expect(tag.render).to eq("<tagname p1='v1' p2='v2' p3='v3' />")
	end
	
	it "can have parameters and children" do
		tag = XMLTagBuilder.new("tagname", {"p1" => "v1", "p2" => "v2", "p3" => "v3"}) do
			_"pff"
		end
		expect(tag.render).to eq("<tagname p1='v1' p2='v2' p3='v3'>" + XMLLeafBuilder.new("pff").render + "</tagname>")
	end
end

describe XMLRootBuilder do
	it "can be empty" do
		root = XMLRootBuilder.new do
		end
		expect(root.render).to eq("")
	end
	
	it "can be initialized with block == nil" do
		root = XMLRootBuilder.new
		expect(root.render).to eq("")
	end
	
	it "renders to joined rendered children" do
		root = XMLRootBuilder.new do
			_"hehe"
			a
			b
		end
		expect(root.render).to eq(
			XMLLeafBuilder.new("hehe").render +
			XMLTagBuilder.new("a").render +
			XMLTagBuilder.new("b").render
		)
	end
end
