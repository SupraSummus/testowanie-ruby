=begin
Klasa tagu wewnątrz którego można podać wiele tagów wyliczających się do tablic
Renderuje się do jednej, płaskiej tablicy zawierającej wyrenderowane dzieci
=end
class ArrayInputElement
	@@childrenTypes = Hash.new()
	
	def initialize(&block)
		@children = Array.new()
		if block != nil
			instance_eval(&block)
		end
	end
	
	def render(context)
		return @children.map{|child| child.render(context)}.flatten(1)
	end
	
	def method_missing(name, *args, &block)
		if @@childrenTypes.has_key?(name)
			@children.push(@@childrenTypes[name].new(*args, &block))
		else
			super
		end
	end
	
	def self.registerChildType(symbol, classType)
		@@childrenTypes.store(symbol, classType)
	end
	
	ArrayInputElement.registerChildType(:array, ArrayInputElement)
end

class TextValueElement
	ArrayInputElement.registerChildType(:textValue, TextValueElement)
	ArrayInputElement.registerChildType(:text, TextValueElement)
	
	def initialize(text)
		@text = text
	end
	
	def render(context)
		return [@text]
	end
end

class JoinElement < ArrayInputElement
	ArrayInputElement.registerChildType(:join, JoinElement)

	def initialize(delimiter = "", &block)
		@delimiter = delimiter
		super(&block)
	end
	
	def render(context)
		return [super.join(@delimiter)]
	end
end

class TextProcessorElement < ArrayInputElement
	def render(context)
		return super.map{|e| process(e)}
	end
end

class ReplaceElement < TextProcessorElement
	ArrayInputElement.registerChildType(:replace, ReplaceElement)

	def initialize(pattern, replacement, &block)
		@pattern = pattern
		@replacement = replacement
		super(&block)
	end
	
	def process(text)
		return text.gsub(@pattern, @replacement)
	end
end

class LowerCaseElement < TextProcessorElement
	ArrayInputElement.registerChildType(:lower, LowerCaseElement)
	
	def process(text)
		return text.downcase
	end
end

=begin
Element który pobiera z MARCa wartość pola (dokładnie: wszystkich pól o podanym tagu)
=end
class MARCFieldElement
	ArrayInputElement.registerChildType(:MARCField, MARCFieldElement)
	
	def initialize(field)
		@field = field
	end
	
	def render(context)
		return \
			context["fields"].
			select{|e| e["tag"] == @field}.
			map{|e| e["value"]}
	end
end

=begin
Element który pobiera z MARCa wartość podpola (dokładnie: wszystkich podpól o podanej tagu pola i kodzie podpola)
=end
class MARCSubfieldElement
	ArrayInputElement.registerChildType(:MARCSubfield, MARCSubfieldElement)
	
	def initialize(field, subfield)
		@field = field
		@subfield = subfield
	end
	
	def render(context)
		return \
			context["fields"]. # wszystkie pola
			select{|e| e["tag"] == @field}. # tylko pola o dobrym tagu
			map{|e| e["data"]["subfields"]}.
			flatten(1). # podpola złączone do jednej tablicy
			select{|e| e["code"] == @subfield}. # tylko podpola o dobrym kodzie
			map{|e| e["data"]}
	end
end

=begin
Bierze z MARCa wartość LEADER
=end
class MARCLeaderElement
	ArrayInputElement.registerChildType(:MARCLeader, MARCLeaderElement)
	
	def render(context)
		return [context["leader"]]
	end
end

class FirstNotEmptyArrayElement < ArrayInputElement
	ArrayInputElement.registerChildType(:firstNotEmpty, FirstNotEmptyArrayElement)
	
	def render(context)
		@children.each{|child|
			out = child.render(context)
			if out.length > 0
				return out
			end
		}
		return []
	end
	
end

class FilterElement < ArrayInputElement
	def render(context)
		return super.select{|e| ok?(e)}
	end
end

class NotEmptyFilterElement < FilterElement
	def ok?(txt)
		return txt != ""
	end
	
	ArrayInputElement.registerChildType(:notEmpty, NotEmptyFilterElement)
end

class NumberFilterElement < FilterElement
	def ok?(txt)
		return !!(txt =~ /^[-+]?[0-9]+$/)
	end
	
	ArrayInputElement.registerChildType(:numbers, NumberFilterElement)
end

class ConditionStatement < ArrayInputElement
	def initialize(&block)
		@conditions = []
		super
	end
	
	def render(context)
		if @conditions.all?{|e| e.render(context)}
			return super
		else
			return []
		end
	end
	
	### elementy języka ###
	
	def equals(string, &block)
		@conditions.push(ConditionEquals.new(string, &block))
	end
	
	def notEquals(string, &block)
		@conditions.push(ConditionNotEquals.new(string, &block))
	end
	
	ConditionStatement.registerChildType(:conditon, ConditionStatement)
end

class ConditionEquals < ArrayInputElement
	def initialize(string, &block)
		@string = string
		super(&block)
	end
	
	def render(context)
		return super == [@string]
	end
end

class ConditionNotEquals < ConditionEquals
	def render(context)
		return !super
	end
end

=begin
Root
=end
class Document
	def initialize(&block)
		@fields = Set.new
		instance_eval(&block)
	end
	
	def render(context)
		r = {fields: {}}
		@fields.each{|field| r[:fields][field.name] = field.render(context)}
		return r
	end
	
	### elementy języka ###
	
	def field(name, &block)
		@fields.add(Field.new(name, &block))
	end
end

=begin

=end
class Field
	attr_reader :name
	
	def initialize(name, &block)
		@name = name
		@values = Set.new
		@attributes = Set.new
		
		if block != nil
			instance_eval(&block)
		end
	end
	
	def render(context)
		r= {
			values: @values.map{|value| value.render(context)}.flatten(1),
			attributes: {}
		}
		# TODO jakos to ładniej zrobic (najlepiej jednolinijkowcem)
		@attributes.each{|attribute|
			r[:attributes][attribute.name] = attribute.render(context)
		}
		return r
	end
	
	### elementy języka
	
	def values(&block)
		@values.add(ValuesElement.new(&block))
	end
	
	def attribute(name, &block)
		@attributes.add(Attribute.new(name, &block))
	end
	
end

class ValuesElement < ArrayInputElement
end

class Attribute < ArrayInputElement
	attr_reader :name
	
	def initialize(name, &block)
		@name = name
		super(&block)
	end
end
