load 'DSL.rb'
require 'json'

$testContext = JSON.parse( IO.read("test_marc.json") )

describe ArrayInputElement do
	it "can be constructed with nil block" do
		e = ArrayInputElement.new
		expect(e.render(nil)).to eq([])
	end
	
	it "can be constructed with empty block" do
		e = ArrayInputElement.new do end
		expect(e.render(nil)).to eq([])
	end
	
	it "can contain other elements" do
		e = ArrayInputElement.new do
			textValue "text"
			textValue "text2"
		end
		expect(e.render(nil)).to eq(["text", "text2"])
	end
end

describe TextValueElement do
	it "evaluates to 1 element array" do
		e = TextValueElement.new("kosmiczna sowa")
		expect(e.render(nil)).to eq(["kosmiczna sowa"])
	end
end

describe JoinElement do
	it "joins with default delimiter = ''" do
		e = JoinElement.new do
			textValue "a"
			textValue "b "
			textValue "c"
		end
		expect(e.render(nil)).to eq(["ab c"])
	end
	
	it "joins with specified delimiter" do
		e = JoinElement.new(" delimiter ") do
			textValue "a"
			textValue "b"
			textValue "c"
		end
		expect(e.render(nil)).to eq(["a delimiter b delimiter c"])
	end
	
	it "joins empty arrays too" do
		e = JoinElement.new(" delimiter ")
		expect(e.render(nil)).to eq([""])
	end
end

describe ReplaceElement do
	it "replaces strings inside all array elements" do
		e = ReplaceElement.new("kot", "pies") do
			textValue "ala ma kota"
			textValue "kot ma ale"
			textValue "ko"
			textValue "t"
			textValue "kotkot"
		end
		expect(e.render(nil)).to eq(["ala ma piesa", "pies ma ale", "ko", "t", "piespies"])
	end
end

describe LowerCaseElement do
	it "converts all array elements to lowercase" do
		e = LowerCaseElement.new do
			textValue "OO"
			textValue "oO"
			textValue "oo"
		end
		expect(e.render(nil)).to eq(["oo", "oo", "oo"])
	end
end

describe MARCLeaderElement do
	it "retrives LEADER from MARC (context)" do
		e = MARCLeaderElement.new
		expect(e.render($testContext)).to eq(["0000nam a2200000 i 4500"])
	end
end

describe MARCFieldElement do
	it "retrives field value from MARC (passed in context)" do
		e = MARCFieldElement.new("008")
		expect(e.render($testContext)).to eq(["080102s1906    pl           |000 0 pol  nam i "])
	end
	
	it "returns empty array when specified tag is not found" do
		e = MARCFieldElement.new("hah")
		expect(e.render($testContext)).to eq([])
	end
	
	it "can handle multiple matching tags" do
		e = MARCFieldElement.new("tst")
		expect(e.render($testContext)).to match_array(["just for tests", "just for tests'"])
	end
end

describe MARCSubfieldElement do
	it "retrives subfield value from MARC" do
		e = MARCSubfieldElement.new("040", "c")
		expect(e.render($testContext)).to eq(["WA N"])
	end
	
	it "can handle multiple matching field tags" do
		e = MARCSubfieldElement.new("700", "t")
		expect(e.render($testContext)).to match_array(["List do Matki Boskiej", "Złoty chleb"])
	end 
	
=begin eee, nie wiem czy to sie kiedykolwiek może zdarzyc. trzeba dopisać ale teraz nie chce mi sie grzebać w testowym jsonie
	it "can handle multipe matching subfield codes" do
		e = 
	end
=end
	
	it "returns empty array when field not found" do
		e = MARCSubfieldElement.new("heh", "c")
		expect(e.render($testContext)).to eq([])
	end
	
	it "returns empty array when subfield not found" do
		e = MARCSubfieldElement.new("700", "x")
		expect(e.render($testContext)).to eq([])
	end
end

describe FirstNotEmptyArrayElement do
	it "returns first not empty array" do
		e = FirstNotEmptyArrayElement.new do
			lower
			array do
				text "a"
				text "b"
			end
			text "c"
		end
		expect(e.render(nil)).to eq(["a", "b"])
	end
	
	it "returns empty array when all children are empty" do
		e = FirstNotEmptyArrayElement.new do
			lower
			array
		end
		expect(e.render(nil)).to eq([])
	end
end

describe NotEmptyFilterElement do
	it "filters empty strings" do
		e = NotEmptyFilterElement.new do
			text ""
			text "a"
			text ""
			text ""
			text "ba"
		end
		expect(e.render(nil)).to eq(["a", "ba"])
	end
end

describe NumberFilterElement do
	it "filters strings that don't represent integers" do
		e = NumberFilterElement.new do
			text "rt"
			text "10"
			text "-9"
			text "0"
			text "ba"
		end
		expect(e.render(nil)).to eq(["10", "-9", "0"])
	end
end

### instrukcje warunkowe ###

describe ConditionEquals do
	it "renders to 'true' if children returned 1 element array containing specified string" do
		cond = ConditionEquals.new "tak" do
			text "tak"
			array do end
		end
		expect(cond.render(nil)).to eq(true)
	end
	
	it "otherwise it renders to 'false'" do
		cond = ConditionEquals.new "tak" do
			text "tak"
			text "tak"
		end
		expect(cond.render(nil)).to eq(false)
	end
	
	it "otherwise it renders to 'false'" do
		cond = ConditionEquals.new "tak" do
			text "nie"
		end
		expect(cond.render(nil)).to eq(false)
	end
	
	it "otherwise it renders to 'false'" do
		cond = ConditionEquals.new "tak"
		expect(cond.render(nil)).to eq(false)
	end
end

describe ConditionNotEquals do
	it "renders to 'false' if children returned 1 element array containing specified string" do
		cond = ConditionNotEquals.new "tak" do
			text "tak"
			array do end
		end
		expect(cond.render(nil)).to eq(false)
	end
	
	it "otherwise it renders to 'true'" do
		cond = ConditionNotEquals.new "tak" do
			text "tak"
			text "tak"
		end
		expect(cond.render(nil)).to eq(true)
	end
	
	it "otherwise it renders to 'true'" do
		cond = ConditionNotEquals.new "tak" do
			text "nie"
		end
		expect(cond.render(nil)).to eq(true)
	end
	
	it "otherwise it renders to 'true'" do
		cond = ConditionNotEquals.new "tak"
		expect(cond.render(nil)).to eq(true)
	end
end

describe ConditionStatement do
	it "renders to joined rendered children when all conditions returned 'true'" do
		e = ConditionStatement.new do
			text "text"
			equals "a" do
				text "a"
			end
			notEquals "b" do end
			text "2text"
		end
		expect(e.render(nil)).to eq(["text", "2text"])
	end
	
	it "renders to empty array otherwise" do
		e = ConditionStatement.new do
			equals "a"
			text "text"
			equals "a" do
				text "a"
			end
			notEquals "b" do end
			text "2text"
		end
		expect(e.render(nil)).to eq([])
	end
end

### testy elementów strukturalnych ###

describe Attribute do
	it "has name and values" do
		a = Attribute.new("attr") do
			text "a"
			text "b"
		end
		expect(a.name).to eq("attr")
		expect(a.render(nil)).to eq(["a", "b"])
	end
	
	it "can have no values" do
		a = Attribute.new("attr")
		expect(a.render(nil)).to eq([])
	end
end

describe ValuesElement do
	it "has values" do
		e = ValuesElement.new do
			text "a"
			text "b"
		end
		expect(e.render(nil)).to eq(["a", "b"])
	end
	
	it "can have no values" do
		e = ValuesElement.new
		expect(e.render(nil)).to eq([])
	end
end

describe Field do
	it "has a name" do
		f = Field.new("potato field")
		expect(f.name).to eq("potato field")
		expect(f.render(nil)).to eq({values: [], attributes: {}})
	end
	
	it "can contain values" do
		f = Field.new("name") do
			values do end
			values do
				text "ex value"
				text "another v"
			end
			values do
				text "phi"
			end
		end
		expect(f.render(nil)).to eq({values: ["ex value", "another v", "phi"], attributes: {}})
	end
	
	it "can have attributes" do 
		f = Field.new("name") do
			attribute :attr1 do
				text "attr1 value"
				text "hyh"
			end
			attribute :attr2 do
				text "attr2 value"
			end
		end
		expect(f.render(nil)).to eq({values: [], attributes: {
			attr1: ["attr1 value", "hyh"],
			attr2: ["attr2 value"]
		}})
	end
end

describe Document do
	it "can be empty" do
		doc = Document.new do end
		expect(doc.render(nil)).to eq({fields: {}})
	end
	
	it "can contain fields" do
		doc = Document.new do
			field :a do end
			field :b do end
		end
		expect(doc.render(nil)).to eq({fields: {
			a: {values: [], attributes: {}},
			b: {values: [], attributes: {}},
		}})
	end
end
